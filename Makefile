.PHONY: test unit-test integration-test run

GO = go

build:
	$(GO) build

test: unit-test integration-test

unit-test:
	@echo ---- Unit Tests ----
	$(GO) test -v -cover
	@echo

integration-test:
	@echo ---- Integration Tests ----
	./tests/integration.sh


run:
	$(GO) run cmd/tftpd/main.go
