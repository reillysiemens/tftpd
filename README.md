In-memory TFTP Server
=====================

This is a simple in-memory TFTP server, implemented in Go.  It is
RFC1350-compliant, but doesn't implement the additions in later RFCs.  In
particular, options are not recognized.

Installation
------------

```bash
git clone https://gitlab.com/reillysiemens/tftpd.git ${GOPATH}/src/igneous.io/tftp
```

Usage
-----
To easily build and run this server a `make` target called `run` has been
provided. Run

```bash
make run
```

in your shell of choice to start the server. This will start the server
of its default port of `6969`. If you wish to set other parameters to the
server such as `interface`, `port`, or `log_path` you can provide them as
command line flags. For example,

```bash
go run cmd/tftpd/main.go -port=9696 -log_path=/tmp/a-different-log-file.log
```

Testing
-------

### Unit Tests
To run unit tests for the code in this repository a `make` target called
`unit-test` has been provided. Run

```bash
make unit-test
```

to run these tests.

- TODO: More robust unit testing.

### Integration Tests

To run the limited integration tests for the code in this repository a `make`
target called `integration-test` has been provided. Run

```bash
make integration-test
```

to run these tests. It will also be necessary to be running the server already
with `make run`.

#### Requirements
- A POSIX shell.
- A `tftp` client.


### All Tests
To run both unit and integration tests a `make` target called `test` has been
provided. Run

```bash
make test
```

to run all tests.

Documentation
-------------
You can use `godoc` to generate library documentation, but it is currently
sparse.

- TODO: Document public interfaces.
