package main

import (
	"flag"
	"igneous.io/tftp"
	"log"
	"os"
)

func parseFlags() (string, int, string) {
	iface := flag.String("interface", "", "the interface listen on")
	port := flag.Int("port", 6969, "the main server port")
	logPath := flag.String("log_path", "./tftpd.log", "the log file path")
	flag.Parse()
	return *iface, *port, *logPath
}

func createLogger(logPath string) (logger *log.Logger, err error) {
	var logFile *os.File
	if logFile, err = os.Create(logPath); err != nil {
		return
	}

	logger = log.New(logFile, "", log.LstdFlags)
	return
}

func main() {
	var (
		server tftp.Server
		logger *log.Logger
		err    error
	)
	iface, port, logPath := parseFlags()

	if logger, err = createLogger(logPath); err != nil {
		log.Fatal(err)
	}

	if server, err = tftp.NewServer(iface, port, logger); err != nil {
		log.Fatal(err)
	}
	server.Serve()
}
