package tftp

const (
	ErrUndefined uint16 = iota
	ErrFileNotFound
	ErrAccessViolation
	ErrDiskFull
	ErrIllegalOperation
	ErrUnkownTID
	ErrFileExists
	ErrNoSuchUser
)
