package tftp

import (
	"bytes"
	"io"
	"sync"
)

type File interface {
	Lock()
	Unlock()
	RLock()
	RUnlock()
	NewReader() io.Reader
	NewWriter() io.Writer
}

type FileMemory struct {
	lock sync.RWMutex
	data bytes.Buffer
}

func (f *FileMemory) Lock() {
	f.lock.Lock()
}

func (f *FileMemory) Unlock() {
	f.lock.Unlock()
}

func (f *FileMemory) RLock() {
	f.lock.RLock()
}

func (f *FileMemory) RUnlock() {
	f.lock.RUnlock()
}

func (f *FileMemory) NewReader() io.Reader {
	return bytes.NewReader(f.data.Bytes())
}

func (f *FileMemory) NewWriter() io.Writer {
	// We should only ever receive requests for a new writer when we intend
	// to write a new file or overwrite an existing one.
	f.data.Truncate(0)
	return &f.data
}
