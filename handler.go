package tftp

import (
	"fmt"
	"io"
	"log"
	"math/rand"
	"net"
	"strconv"
	"time"
)

const (
	// IANA recommended port range for dynamic ports. 16,383 ports.
	lowEphemeralPort  = 49152 // 2^15 + 2^14
	highEphemeralPort = 65535 // 2^16 - 1

	chunkSize = 512 // File chunk size in bytes.

	readDeadline  = 5 * time.Second // UDP read deadline.
	writeDeadline = 5 * time.Second // UDP write deadline.
)

func randomEphemeralPort() int {
	return rand.Intn(highEphemeralPort-lowEphemeralPort) + lowEphemeralPort
}

func randomAddr(iface string) (addr *net.UDPAddr, err error) {
	port := randomEphemeralPort()
	addr, err = net.ResolveUDPAddr("udp", iface+":"+strconv.Itoa(port))
	return
}

type Handler struct {
	addr     *net.UDPAddr
	conn     *net.UDPConn
	client   *net.UDPAddr
	file     File
	blockNum uint16
}

func (h *Handler) sendPacketTo(packet Packet, addr *net.UDPAddr) (err error) {
	deadline := time.Now().Add(writeDeadline)

	// Don't wait too long for the write operation to complete.
	if err = h.conn.SetWriteDeadline(deadline); err != nil {
		return
	}

	_, err = h.conn.WriteToUDP(packet.Serialize(), addr)
	return
}

func (h *Handler) sendPacket(packet Packet) (err error) {
	return h.sendPacketTo(packet, h.client)
}

func (h *Handler) recvPacket() (packet Packet, err error) {
	var (
		bytesRead int
		addr      *net.UDPAddr
	)
	data := make([]byte, MaxPacketSize)
	deadline := time.Now().Add(readDeadline)

	// Don't wait too long for the read operation to complete.
	if err = h.conn.SetReadDeadline(deadline); err != nil {
		return
	}

	if bytesRead, addr, err = h.conn.ReadFromUDP(data); err != nil {
		return
	}

	// TODO: There has *got* to be a better way to compare UDPAddrs... 🤔
	if addr.String() != h.client.String() {
		packet := &PacketError{ErrUnkownTID, "unknown TID"}
		h.sendPacketTo(packet, addr)
	}

	packet, err = ParsePacket(data[:bytesRead])
	return
}

func (h *Handler) sendData(data []byte) (err error) {
	packet := &PacketData{h.blockNum, data}
	err = h.sendPacket(packet)
	return
}

func (h *Handler) recvData() (blockNum uint16, data []byte, err error) {
	var packet Packet
	if packet, err = h.recvPacket(); err != nil {
		return
	}

	switch p := packet.(type) {
	case *PacketData:
		return p.BlockNum, p.Data, nil
	case *PacketError:
		return 0, nil, fmt.Errorf("error code %d: %s", p.Code, p.Msg)
	default:
		return 0, nil, fmt.Errorf("unexpected response packet")
	}
}

func (h *Handler) sendAck() (err error) {
	packet := &PacketAck{h.blockNum}
	err = h.sendPacket(packet)
	return
}

func (h *Handler) recvAck() (blockNum uint16, err error) {
	var packet Packet
	if packet, err = h.recvPacket(); err != nil {
		return
	}

	switch p := packet.(type) {
	case *PacketAck:
		return p.BlockNum, nil
	case *PacketError:
		return 0, fmt.Errorf("error code %d: %s", p.Code, p.Msg)
	default:
		return 0, fmt.Errorf("unexpected response packet")
	}
}

func (h *Handler) writeFile() (err error) {
	var (
		recvBlockNum uint16
		bytesRead    int
	)
	h.blockNum = 1 // Block number begins at 1 for transfer to client.
	reader := h.file.NewReader()
	chunk := make([]byte, chunkSize)

	for {
		// Read a chunk from the file.
		if bytesRead, err = reader.Read(chunk); err == io.EOF {
			break // There are no more chunks to read from the file.
		} else if err != nil {
			return // An unexpected error occurred while reading from the file.
		}

		// Send the chunk that was read. Receive and validate the ACK. Attempt
		// retransmit if the block number is invalid. Will timeout eventually.
		for {
			if err = h.sendData(chunk[:bytesRead]); err != nil {
				return
			}

			if recvBlockNum, err = h.recvAck(); err != nil {
				return
			}

			if recvBlockNum == h.blockNum {
				break
			} else {
				log.Println(h.client, "ERR", "expected block", h.blockNum, ", received", recvBlockNum)
			}
		}

		// We've finished sending and acknowledging a single chunk.
		h.blockNum += 1
	}

	return nil
}

func (h *Handler) readFile() (err error) {
	var (
		recvBlockNum uint16
		data         []byte
	)
	writer := h.file.NewWriter()

	// Send an initial acknowledgement for the data received.
	if err = h.sendAck(); err != nil {
		return
	}

	for {

		h.blockNum += 1

		// Receive and validate the data. Attempt retransmit if the block
		// number is invalid. Send an ACK if the data is good. Will timeout
		// eventually.
		for {

			if recvBlockNum, data, err = h.recvData(); err != nil {
				return
			}

			if recvBlockNum == h.blockNum {
				if err = h.sendAck(); err != nil {
					return
				}

				if _, err = writer.Write(data); err != nil {
					return
				}

				break // Exit the inner loop to increment the block number.
			} else {
				log.Println(h.client, "ERR", "expected block", h.blockNum, ", received", recvBlockNum)
			}
		}

		// The read is finished when less than a chunk was read.
		if len(data) < chunkSize {
			return nil
		}
	}

	return nil
}

func (h *Handler) HandleRRQ() {
	var err error

	// Start listening on our random port. Close when finished.
	if h.conn, err = net.ListenUDP("udp", h.addr); err != nil {
		log.Println(err)
		return
	}
	defer h.conn.Close()

	if h.file == nil {
		h.sendPacket(&PacketError{ErrFileNotFound, "file not found"})
		return
	}

	// Lock the file for reading. Unlock it when finished.
	h.file.RLock()
	defer h.file.RUnlock()

	// Write the file to the remote connection.
	if err = h.writeFile(); err != nil {
		log.Println(h.client, "ERR", err)
	}
}

func (h *Handler) HandleWRQ() {
	var err error

	// Start listening on our random port. Close when finished.
	if h.conn, err = net.ListenUDP("udp", h.addr); err != nil {
		log.Println(err)
		return
	}
	defer h.conn.Close()

	// Lock the file for writing. Unlock it when finished.
	h.file.Lock()
	defer h.file.Unlock()

	// Read the file from the remote connection.
	if err = h.readFile(); err != nil {
		log.Println(h.client, "ERR", err)
	}
}

func NewHandler(iface string, client *net.UDPAddr, file File) (handler Handler, err error) {
	if addr, err := randomAddr(iface); err == nil {
		handler = Handler{addr: addr, client: client, file: file}
	}
	return
}
