package tftp

import (
	"log"
	"net"
	"strconv"
)

type Server struct {
	addr   *net.UDPAddr
	files  map[string]File
	logger *log.Logger
}

func (s *Server) handle(client *net.UDPAddr, packet Packet) (err error) {
	switch p := packet.(type) {
	case *PacketRequest:
		file, exists := s.files[p.Filename]

		handler, err := NewHandler(string(s.addr.IP), client, file)
		if err != nil {
			return err
		}

		switch p.Op {
		case OpRRQ: // Spawn RRQ handler.
			log.Println(client, "RRQ", p.Filename)
			s.logger.Println(client, "RRQ", p.Filename)
			go handler.HandleRRQ()
		case OpWRQ: // Spawn WRQ handler.
			log.Println(client, "WRQ", p.Filename)
			s.logger.Println(client, "WRQ", p.Filename)
			if !exists {
				handler.file = new(FileMemory)
				s.files[p.Filename] = handler.file
			}
			go handler.HandleWRQ()
		}
	default:
		log.Println(client, "ERR", "unexpected packet")
	}

	return nil
}

func (s *Server) Serve() {
	conn, err := net.ListenUDP("udp", s.addr)
	if err != nil {
		log.Println(err)
		return
	}
	defer conn.Close()

	log.Println("listening on", s.addr)

	buf := make([]byte, MaxPacketSize)

	for {
		bytesRead, client, err := conn.ReadFromUDP(buf)
		if err != nil {
			log.Println(err)
		}

		packet, err := ParsePacket(buf[0:bytesRead])
		if err != nil {
			log.Println(client, "ERR", err)
			s.logger.Println(client, "ERR", err)
		} else {
			if err := s.handle(client, packet); err != nil {
				log.Println(err)
			}
		}
	}
}

func NewServer(iface string, port int, logger *log.Logger) (server Server, err error) {
	addr := iface + ":" + strconv.Itoa(port)
	if addr, err := net.ResolveUDPAddr("udp", addr); err == nil {
		server = Server{addr, make(map[string]File), logger}
	}
	return
}
