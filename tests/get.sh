#!/usr/bin/env sh

tftp_get() {
    local remote_file=$1
    local local_file=$2

    tftp -v ${INTERFACE} ${PORT} -m octet -c get ${remote_file} ${local_file}
}

tftp_get_random() {
    local remote_file=$1
    local num_gets=$2

    for i in $(seq 1 $num_gets)
    do
        local_file="/tmp/$(shuf -n1 ${WORD_FILE}).txt"
        tftp_get ${remote_file} ${local_file} &
    done

    wait
}
