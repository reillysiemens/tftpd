#!/usr/bin/env sh
test_dir=$(dirname $0)

source "${test_dir}/get.sh"
source "${test_dir}/put.sh"

: ${INTERFACE:="127.0.0.1"}
: ${PORT:=6969}
: ${WORD_FILE:="/usr/share/dict/words"}

put_and_get() {
    local_file="/usr/share/dict/words"
    remote_file="words.txt"
    tmp_file="/tmp/words.txt"

    tftp_put ${local_file} ${remote_file}
    tftp_get ${remote_file} ${tmp_file}

    head -n1 ${local_file} > ${tmp_file}

    [ -z "$(diff ${local_file} ${tmp_file})" ]
}

get_random() {
    remote_file="words.txt"
    num_gets=100
    tftp_get_random $remote_file $num_gets
}

put_random() {
    local_file="/usr/share/dict/words"
    num_puts=100
    tftp_put_random $local_file $num_puts
}

put_and_get
get_random
put_random
