#!/usr/bin/env sh

tftp_put() {
    local local_file=$1
    local remote_file=$2

    tftp -v ${INTERFACE} ${PORT} -m octet -c put ${local_file} ${remote_file}
}

tftp_put_random() {
    local local_file=$1
    local num_gets=$2

    for i in $(seq 1 $num_gets)
    do
        remote_file="/tmp/$(shuf -n1 ${WORD_FILE}).txt"
        tftp_put ${local_file} ${remote_file} &
    done

    wait
}
